<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->username,
            'email' => $this->faker->unique()->safeEmail,
            'birthdate' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'gender' => (bool)random_int(0, 1),
            'job' => $this->faker->jobTitle,
            'location' => $this->faker->city
        ];
    }
}
