<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center  header-transparent ">
    <div class="container d-flex align-items-center">

        <div class="logo mr-auto">
            <h1 class="text-light"><a href="{{ route('index') }}">Andrade y asociados</a></h1>
        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="{{ Request::is('/') ? 'active' : '' }}">
                    <a href="{{ route('index') }}">Home</a>
                </li>
                <li>
                    <a href="#about">About</a>
                </li>
                <li>
                    <a href="#services">Services</a>
                </li>
                <li>
                    <a href="#contact">Contact</a>
                </li>
                <li class="drop-down {{ Request::is('client*') ? 'active' : '' }}">
                    <a href="#">Clients</a>
                    <ul>
                        <li><a href="{{ route('client.index') }}">All Clients</a></li>
                        <li><a href="{{ route('client.create') }}">New Client</a></li>
                    </ul>
                </li>
                <li class="{{ Request::is('agenda') ? 'active' : '' }}">
                    <a href="{{ route('agenda') }}">Agenda</a>
                </li>
                <li class="drop-down {{ Request::is('login', 'register') ? 'active' : '' }}">
                    <a href="#">Login / Register</a>
                    <ul>
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    </ul>
                </li>
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->