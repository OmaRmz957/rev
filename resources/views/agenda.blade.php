@extends('layouts.guest')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/mark-your-calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pay.css') }}">    
@endsection

@section('content')
    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Agenda</h2>
        <ol>
          <li><a href="index.html">Home</a></li>
          <li>Agenda</li>
        </ol>
      </div>

    </div>
  </section>
  <!-- End Breadcrumbs -->

  <section class="inner-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div id="picker"></div>
            </div>

            <div class="col-md-8 d-none" id="pay">
                <form onsubmit="event.preventDefault()" class="form-card">
                    <div class="row justify-content-center mb-4 radio-group">
                        <div class="col-sm-3 col-5">
                            <div class='radio mx-auto' data-value="visa"> <img class="fit-image" src="https://i.imgur.com/OdxcctP.jpg" width="105px" height="55px"> </div>
                        </div>
                        <div class="col-sm-3 col-5">
                            <div class='radio mx-auto' data-value="master"> <img class="fit-image" src="https://i.imgur.com/WIAP9Ku.jpg" width="105px" height="55px"> </div>
                        </div>
                        <div class="col-sm-3 col-5">
                            <div class='radio mx-auto' data-value="paypal"> <img class="fit-image" src="https://i.imgur.com/cMk1MtK.jpg" width="105px" height="55px"> </div>
                        </div> <br>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="input-group"> <input type="text" name="Name" placeholder="John Doe"> <label>Name</label> </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="input-group"> <input type="text" id="cr_no" name="card-no" placeholder="0000 0000 0000 0000" minlength="19" maxlength="19"> <label>Card Number</label> </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group"> <input type="text" id="exp" name="expdate" placeholder="MM/YY" minlength="5" maxlength="5"> <label>Expiry Date</label> </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group"> <input type="password" name="cvv" placeholder="&#9679;&#9679;&#9679;" minlength="3" maxlength="3"> <label>CVV</label> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row justify-content-center">
                        <div class="col-md-12"> 
                            <input type="submit" value="Pay" class="btn btn-success btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </section>
@endsection

@section('js')
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/mark-your-calendar.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript">
        (function($) {
        $('#picker').markyourcalendar({
            availability: [
            ['1:00', '2:00', '3:00', '4:00', '5:00'],
            ['2:00'],
            ['3:00'],
            ['4:00'],
            ['5:00'],
            ['6:00'],
            ['7:00']
            ],
            startDate: new Date(),
            onClick: function(ev, data) {
                // data is a list of datetimes
                var d = data[0].split(' ')[0];
                var t = data[0].split(' ')[1];
                $('#selected-date').html(d);
                $('#selected-time').html(t);
                $('#pay').removeClass('d-none');
            },
            onClickNavigator: function(ev, instance) {
                var arr = [
                    [
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['1:00', '5:00'],
                    ['2:00', '5:00'],
                    ['3:30'],
                    ['2:00', '5:00'],
                    ['2:00', '5:00'],
                    ['2:00', '5:00']
                    ],
                    [
                    ['2:00', '5:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['4:00', '5:00'],
                    ['2:00', '5:00'],
                    ['2:00', '5:00'],
                    ['2:00', '5:00'],
                    ['2:00', '5:00']
                    ],
                    [
                    ['4:00', '5:00'],
                    ['4:00', '5:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00']
                    ],
                    [
                    ['4:00', '5:00'],
                    ['4:00', '5:00'],
                    ['4:00', '5:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['4:00', '5:00'],
                    ['4:00', '5:00'],
                    ['4:00', '5:00']
                    ],
                    [
                    ['4:00', '6:00'],
                    ['4:00', '6:00'],
                    ['4:00', '6:00'],
                    ['4:00', '6:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['4:00', '6:00'],
                    ['4:00', '6:00']
                    ],
                    [
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['3:00', '6:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00'],
                    ['3:00', '6:00']
                    ],
                    [
                    ['3:00', '4:00'],
                    ['3:00', '4:00'],
                    ['3:00', '4:00'],
                    ['3:00', '4:00'],
                    ['3:00', '4:00'],
                    ['3:00', '4:00'],
                    ['4:00', '5:00', '6:00', '7:00', '8:00']
                    ]
                ]
                var rn = Math.floor(Math.random() * 10) % 7;
                instance.setAvailability(arr[rn]);
            }
        });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('assets/js/pay.js') }}"></script>
@endsection