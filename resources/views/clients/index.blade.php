@extends('layouts.guest')

@section('content')
    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Clients</h2>
        <ol>
          <li><a href="index.html">Home</a></li>
          <li>Clients</li>
        </ol>
      </div>

    </div>
  </section>
  <!-- End Breadcrumbs -->

  <section class="inner-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
  </section>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
@endpush