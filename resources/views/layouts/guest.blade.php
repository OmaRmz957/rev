<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        @include('partials.styles')

        @yield('css')
    </head>

    <body>

        <div id="app">

            @include('partials.header')

            <main id="main" >

                @yield('content')

            </main><!-- End #main -->   

            @include('partials.footer')

            @include('partials.scripts')

        </div>

        <script src="{{ mix('js/app.js') }}"></script>
        <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
        @stack('scripts')
        @yield('js')

    </body>
</html>
