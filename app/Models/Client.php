<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['birthdate'])
            ->diff(\Carbon\Carbon::now())
            ->format('%y years, %m months');
    }
}
