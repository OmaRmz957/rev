<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'partials.index')->name('index');

Auth::routes();

Route::resource('client', ClientController::class);

Route::view('agenda', 'agenda')->name('agenda');

Route::post('task', [TaskController::class, 'store'])
    ->name('task.store');

Route::get('/home', [HomeController::class, 'index'])
    ->name('home');
